/* 
Arrange
Act
Assert
*/
describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.visit('/')
    cy.contains('h1', 'Welcome to Your Vue.js App')
  })

  it('check tag', () => {
    cy.get('[data-cy=installed]').contains('Installed')
  })

  it('clicks docs link for vue.js', () => {
    cy.contains('documentation').click()
  })
})
